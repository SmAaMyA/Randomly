import math
import time

t1 = time.time()

result = 0
for i in range(1, 10000):
    for j in range(1, 10000):
        result = (result + math.log(i + j)) / 2

t2 = time.time()

print(result)
print('time: ' + str(t2 - t1))
