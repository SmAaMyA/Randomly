import itertools
import time

import numpy as np

t1 = time.time()

result = 0
for (i, j) in itertools.product(np.arange(1, 10000), np.arange(1, 10000)):
    result = (result + np.log(i + j)) / 2

t2 = time.time()

print(result)
print('time: ' + str(t2 - t1))
