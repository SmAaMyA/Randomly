import time


def __recursive(n, steps):
    way = 0
    stack = [n]

    while stack:
        cur = stack.pop()
        for step in steps:
            remainder = cur - step
            if remainder == 0:
                way += 1
            elif remainder > 0:
                stack.append(remainder)

    return way


def __count(n, start, stop, steps, resolution):
    size = len(resolution)

    if start < stop <= size:
        for i in range(start, stop):
            way = 0
            for step in steps:
                way += resolution[i - step]
            resolution[i] = way

    return resolution[n]


def count_ways(n, steps):
    size = n + 1
    resolution = [0] * size
    max_step = max(steps)
    i = 0

    # TODO: Fix this block to compute at any steps
    while i <= max_step and i < size:
        resolution[i] = __recursive(i, steps)
        i += 1

    return __count(n, max_step + 1, size, steps, resolution)


def main():
    debug = False
    steps = [1, 4, 5]  # Change steps here

    if debug:
        for n in range(1, 31):
            print('n = ' + str(n))
            s = time.perf_counter()
            w = __recursive(n, steps)
            e = time.perf_counter()
            print('way = ' + str(w) + ' time = ' + str(e - s) + ' sec | ', end='', flush=True)
            s = time.perf_counter()
            w = count_ways(n, steps)
            e = time.perf_counter()
            print('way = ' + str(w) + ' time = ' + str(e - s) + ' sec')
    else:
        n = eval(input('n = '))
        s = time.perf_counter()
        w = count_ways(n, steps)
        e = time.perf_counter()
        print('way = ' + str(w) + '\ntime = ' + str(e - s) + ' sec')


if __name__ == '__main__':
    main()
